<!-- markdownlint-disable MD024 -->
# KPET - Kernel Patch-Evaluated Testing

![KPET logo](logo.png)

KPET is a framework which will execute targeted testing based on changes introduced
in the patch, e.g. a network driver or similar would trigger network related testing
to be invoked, or a filesystem change would invoke filesystem testing.  

[[_TOC_]]

## Install KPET

`kpet` is written in **Python** that means you will need `python3` installed in your
system. You can execute it directly from the repository via `python3 -m kpet`
or you can install it via pip and use it directly from your `PATH`.

### Install the last version from the official repository

```bash
pip install --user --upgrade git+https://gitlab.com/cki-project/kpet.git/
```

> NOTE: The flag `--upgrade` (or `-U` ) is needed to make sure you install
> the last version.

### Install from the local repository (for usage)

Go to the root of this repository and run:

```bash
pip install --user .
```

### Install from the local repository (for testing and development)

For this you'll need to install the normal dependencies, but also the development
dependencies.
Go to the root of this repository and run:

```bash
pip install --user -e .[dev]
```

> NOTE: The `-e` flag stands for `editable mode`, which means that it won't
> actually install the package's files, but a special file (`kpet.egg-link`)
> which points to the repository.
>
> The dependencies will be installed and the `kpet` package will be also _installed_,
> in a way. This is useful for testing the local changes without the need to
> re-install it every time.

### Uninstall

If you've chosen one of the first two options, to uninstall the package will be
as any normal Python package:

```bash
pip uninstall kpet
```

But if you've chosen the third option (`-e` or `editable mode`), that method won't work.
Instead, you need to do the following:

```bash
python3 setup.py develop -u
```

## Install KPET Database

In order to use `kpet` you will need to download the kpet database which includes the
templates and pattern files needed to analyze the patch and generate the corresponding
beaker xml.

```bash
git clone https://gitlab.com/redhat/centos-stream/tests/kernel/kpet-db.git
```

## How to run it

To preview patch generated test cases:

```bash
kpet run test list 001.patch
```

To generate complete Beaker XML:

```bash
kpet run generate --description 'skt ##KVER##' -a aarch64 -k '##KPG_URL##' -t upstream 001.patch
```

You have to run these commands in the kpet database directory, or specify
the `--db` option to point to the kpet database directory.

## Command-line interface

The general format of a kpet command line is:

```text
kpet [GLOBAL_OPTION]... NOUN... VERB [VERB_OPTION]...
```

where `VERB` operates on objects specified by a series of `NOUN`s.

A `NOUN` is an object which could be manipulated by kpet, e.g. `tree`, `arch`,
or `run`, and a verb is an action on that object, e.g. `list` or `generate`.

So, for example:

```text
kpet tree list
```

would list kernel trees known to the database. And e.g.:

```text
kpet run generate
```

would generate XML describing a test run.

Nouns (objects) could be nested. E.g.

```text
kpet run test list
```

would list tests included into a run.

Some of the **nouns** (in bold), **_verbs_** (in bold italic) and `options`
(monospace) are described below.

### Global options

#### `--db DB`

Specify the location of a kpet database ("kpet-db"). The database is assumed
to reside in the current working directory, if the option is not specified.

### Nouns and verbs

#### run

A test run. An execution of tests in the database on a particular
architecture, for a particular kernel tree and multiple other conditions and
parameters. You can generate an XML file describing a run with `generate` and
you can operate on run's `test`s, and `source`s.

##### _generate_

Generate an XML file describing a run. The resulting file can be supplied to
a [Beaker](https://beaker-project.org/) instance for execution. Accepts a
multitude of options, which are split into two groups: "global parameters",
and "execution parameters".

Execution parameters can be captured by the `-e`/`--execute` option to
produce an extra execution of all the tests in the database. The
`-e`/`--execute` option only takes into account execution parameters to the
left of itself. Any changes to those parameters to the right will only affect
following executions. Global parameters are shared by all executions of the
tests, regardless of the `-e`/`--execute` option use.

###### Execution parameters

* `-a ARCH`, `--arch ARCH`

  Architecture of the specified kernel. Required.
  Run `kpet arch list` to see supported architectures.

* `-t TREE`, `--tree TREE`

  Name of the specified kernel's tree. Required.
  Run `kpet tree list` to see recognized trees.

* `-k KERNEL`, `--kernel KERNEL`

  Location of the kernel to be tested. How the `KERNEL` string is interpreted
  is up to the database, but the in the current kpet-db output it is supplied
  to a
  [script](https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests/-/blob/main/distribution/kpkginstall/runtest.sh)
  executed in Beaker. Among other things, it could be a URL pointing to a
  kernel image tarball, or a yum/dnf repository containing kernel packages.

* `-c REGEX`, `--components REGEX`

  Specify a regular expression matching names of extra components included
  into the kernel build. Run `kpet component list` to see known components and
  their descriptions. Used to determine which tests can run with the build.
  If not specified, the build is considered to contain no extra components,
  just the kernel image.

* `-s PATTERN`, `--sets PATTERN`

  Specify a test set pattern: regexes (fully) matching names of test sets
  to restrict the run to, combined using `&`, `|`, `!` and `()` operators.
  The operators and whitespace can be included into regexes, if escaped with a
  backslash (`\\`). Run `kpet set list` to see known sets and their
  descriptions. If not specified, tests are not limited by their set
  membership.

  Examples:
  * `stor` - run storage tests only,
  * `kt0 & net` - restrict the run to "kt0" networking tests,
  * `kt1 & !kt0` - run tests that are in "kt1", but not in "kt0",
  * `kt1 & (net | fs)` - run "kt1" networking and filesystem tests.

* `--tests REGEX`

  Specify a regular expression matching names of the tests to limit the run
  to. If not specified, tests are not limited by name.

* `--domains REGEX`

  A regular expression matching slash-separated paths of domains to restrict
  the run to. Only host types belonging to the matching domains (and their
  subdomains) will be available to tests. Not allowed, if the database has no
  domains defined. Run `kpet domain tree` to see all available domains.

* `--targeted [{false,no,ignore,true,yes}]`

  Only include tests targeting specified sources, if specified without value,
  or with value "true"/"yes". Only include tests not targeting specified
  sources, if specified with value "false"/"no". Include tests regardless of
  their targeted status, if not specified, or specified with value "ignore".

* `--triggered [{false,no,ignore,true,yes}]`

  Only include tests triggered by specified sources, if specified without
  value, or with value "true"/"yes". Only include tests not triggered by
  specified sources, if specified with value "false"/"no". Include tests
  regardless of their triggered status, if not specified, or specified with
  value "ignore".

* `-e`, `--execute`

  Commit to an execution of the tests with execution parameters on the left.
  Parameter changes on the right will only affect the following executions.

  All the options except `-e`/`--execute` behave normally. That is, repeating
  an option anywhere on the command line simply overrides the previous value.
  What the `-e`/`--execute` option does is it takes a snapshot of the current
  ("execute parameter") option values, and adds an execution of the tests
  based on them.

Tests are executed only once without the `-e`/`--execute` option. E.g.:

```bash
kpet run generate --tree rhel8 \
                  --arch x86_64 \
                  --kernel https://example.com/kernel.x86_64.tar.gz
```

Each occurrence of the option adds another execution. E.g. this would execute
tests for two builds of a RHEL8 kernel:

```bash
kpet run generate --tree rhel8 \
                  --arch x86_64 \
                  --kernel https://example.com/kernel.x86_64.tar.gz \
                  --execute \
                  --arch aarch64 \
                  --kernel https://example.com/kernel.aarch64.tar.gz
```

The `--tree` option sets the tree to `rhel8`, and that is picked up for the
execution produced by the `--execute` option, as well as for the implicit
execution at the end of the command line.

The first `--arch` option sets the architecture to `x86_64` and that is used
by the `--execute` option, but the second `--arch` option changes it to
`aarch64` for the implicit execution. The `--kernel` options behave in the
same way to specify different kernel tarballs for the two executions.

The two commands below would do the same testing as the above, but would
generate two separate Beaker job XMLs:

```bash
kpet run generate --tree rhel8 \
                  --arch x86_64 \
                  --kernel https://example.com/kernel.x86_64.tar.gz
kpet run generate --tree rhel8 \
                  --arch aarch64 \
                  --kernel https://example.com/kernel.aarch64.tar.gz
```

###### Global parameters

* `-v NAME=VALUE`, `--variable NAME=VALUE`

  Specify a variable value to templates generating the output XML.
  Run `kpet variable list` to see names of accepted variables and their
  descriptions.

* `-d DESCRIPTION`, `--description DESCRIPTION`

  Specify an arbitrary string describing the run. With the current kpet-db
  this will appear on the Beaker job description ("whiteboard").

You can also specify kernel patches as positional arguments, and kpet will
pick tests covering the source files changed by those patches. If no patches
are specified, every source file will be considered changed.

Example:

```bash
kpet run generate -a x86_64 -t rhel8 \
                  -k https://example.com/kernel.tar.gz
```

The above would generate a run for a kernel available at
`https://example.com/kernel.tar.gz`, and built from the RHEL8 tree, for x86_64
architecture.

This would do the same, but only running tests covering source files changed
in `new_feature.patch` or `a_fix.patch`:

```bash
kpet run generate -a x86_64 -t rhel8 \
                  -k https://example.com/kernel.tar.gz \
                  new_feature.patch a_fix.patch
```

##### test

A test within a run. You can list them with `list`.

###### _list_

List the tests picked for a run. Accepts all the `kpet run generate` options,
except those not required for selecting tests. Example:

```text
kpet run test list -a x86_64 -t rhel8 new_feature.patch a_fix.patch
```

##### source

A source file which change is being tested within a run. You can list them with `list`.

###### _list_

List the source files which changes are being tested by a run.
Accepts all the `kpet run generate` options, except those not required for
selecting tests, and plus two options for including/excluding "triggered" and
"targeted" sources: `--triggered-sources`/`--targeted-sources`. Example:

```text
kpet run source list -a x86_64 -t rhel8 --targeted-sources=yes \
                     new_feature.patch a_fix.patch
```

#### test

A test described by the database. Can be listed with `list`.

##### _list_

List tests in the database. Accepts multiple options, including:

* `-t REGEX`, `--trees REGEX`

  A regular expression matching the names of kernel trees, which listed tests
  should match. Run `kpet tree list` to see recognized trees. If not provided,
  tree conditions won't affect filtering.

* `-a REGEX`, `--arches REGEX`

  A regular expression matching the names of architectures, which listed tests
  should match. Run `kpet arch list` to see recognized architectures. If not
  provided, architecture conditions won't affect filtering.

* `-c REGEX`, `--components REGEX`

  A regular expression matching extra components included into the kernel
  build, which listed tests should match. Run `kpet component list` to see
  known components and their descriptions. If not provided, component
  conditions won't affect filtering.

* `-s PATTERN`, `--sets PATTERN`

  A test set pattern: regexes (fully) matching names of test sets,
  that listed tests should belong to, combined using `&`, `|`, `!` and `()`
  operators. The operators and whitespace can be included into regexes, if
  escaped with a backslash (`\\`). Run `kpet set list` to see known sets and
  their descriptions. If not specified, tests are not limited by their set
  membership.

  Examples:
  * `stor` - list storage tests only,
  * `kt0 & net` - list "kt0" networking tests,
  * `kt1 & !kt0` - list tests that are in "kt1", but not in "kt0",
  * `kt1 & (net | fs)` - list "kt1" networking and filesystem tests.

* `--tests REGEX`

  A regular expression (fully) matching names of tests to list. If not
  provided, tests won't be filtered by name.

The command also accepts patch file names or URLs as positional arguments.
Providing a patch would limit the output to tests which cover the source code
files changed by the patch.

Example:

```text
kpet test list -a x86_64 -t 'rhel8.*' -s kt0 new_feature.patch
```

#### tree

A kernel "tree". E.g. `rhel8`, `rhel7`, or `upstream`. Can be listed with
`list`.

##### _list_

List the trees described in the database. E.g.:

```text
kpet tree list
```

#### arch

A kernel's target architecture. E.g. `x86_64`, or `aarch64`. Can be listed
with `list`.

##### _list_

List the architectures described in the database. E.g.:

```text
kpet arch list
```

#### component

A build component. E.g. `headers`, or `debuginfo`. Can be listed with `list`.

##### _list_

List the build components described in the database. E.g.:

```text
kpet component list
```

#### set

A test set. E.g. `fs`, `mem`, or `net`. Can be listed with `list`.

##### _list_

List the test sets described in the database. E.g.:

```text
kpet set list
```

#### variable

An XML template's variable. E.g. `job_group`, or `coverage`. Can be listed
with `list`.

##### _list_

List the template variables described in the database. E.g.:

```text
kpet variable list
```

#### domain

A host "domain" - a collection of hosts to run tests on.

##### _list_

List the domains described in the database. E.g.:

```text
kpet domain list
```

## Design principles

For now we only care about compatibility with our kpet-db, which
means that we can break compatibility by following these steps:

1. We add support for whatever new format, while keeping support for
   the old/current format.
1. We modify kpet-db to follow the new format.
1. We can safely drop support for the old format and only keep the
   new.

## Exported variables for kpet database templates

The following variables are passed to the Beaker templates in the kpet
database. They effectively are the interface between kpet and
whichever database you decide to use:

* `DESCRIPTION`: Description for the Beaker test run.
* `SCENES`: A list of "scenes", each describing a particular execution of
  tests in the database.
* `VARIABLES`: Dictionary with extra template variables.

Each scene object in the "SCENES" list has the following attributes:

* `kernel`: Location of the kernel to be tested. Has no meaning to kpet and is
  up to interpretation by the output template.
* `arch`: The name of the architecture the kernel tested in the scene was
  built for.
* `tree`: The name of the source code tree the kernel tested in the scene
  comes from.
* `components`: A list of names of the extra build components the kernel
  tested in the scene contains.
* `recipesets`: The list of recipe sets in the scene. See below for details.

Each recipe set is a _list_ of "host" objects containing these attributes:

* `type_name`: Name of the type of the host, a key from the "host_types"
  dictionary in database's top `index.yaml` file.
* `description`: A human-readable description assigned to the host's
  type (previously-used `type_description` is deprecated now).
* `hostname`: Hostname if this will run on a specific machine, None otherwise.
* `condition`: A logical expression describing the condition the provisioned
  host should satisfy, combining hostnames and hostRequires template filenames
  using AND, OR, and NOT operations. The expression is either:
  * a string starting with `T:`, followed by a path to a file with Jinja2
    template rendering to host requirements;
  * a string starting with `H:`, followed by an FQDN of a host;
  * a list of expressions, combined with AND operator (the default),
    or with OR operator (if the list is the value for `or` key in a
    dictionary)
  * a dictionary of string keys and expression values, where each key
    can be one of the following:
    * `and` - the expression values contained in a dictionary or a
      list value should be combined using the AND operator (no
      effect on other values);
    * `or` - the expression values contained in a dictionary or a list
      value should be combined using the OR operator (no effect on
      other values);
    * `not` - the expression value should be negated;
* `ignore_panic`: If kernel panics should be ignored when running
  tests. Copied from the kpet database value of the same name.
* `partitions_list`: a list of paths to Jinja2 templates with custom partition
  configuration. Contains the `partitions` database values of the host's type,
  and all the suites and cases executed on the host. See
  <https://beaker-project.org/docs/user-guide/customizing-partitions.html> for
  more information.
* `kickstart_list`: a list of paths to Jinja2 templates with custom Anaconda
  kickstart configuration. Contains the `kickstart` database values of the
  host's type, and all the suites and cases executed on the host. See
  <https://beaker-project.org/docs/admin-guide/kickstarts.html> for more
  information.
* `kernel_options`: additional kernel options to be used during Anaconda
  installation.
* `kernel_options_post`: additional kernel options to be used after Anaconda
  installation.
* `preboot_tasks`: path to Jinja2 template to include into the "recipe"
  element, right before the task booting the kernel being tested. Relative to
  the top directory.
* `postboot_tasks`: path to Jinja2 template to include into the "recipe"
  element, right after the task booting the kernel being tested. Relative to
  the top directory.
* `tests`: List of tests.
* `guests`: A dictionary of guest names and data.

Each test is an object with the following attributes:

* `name`: Name of the test.
* `universal_id`: Universally-recognized ID of the test.
  E.g. a [KCIDB](https://github.com/kernelci/kcidb/) test ID.
* `origin`: The name of a test origin - the source for the test's code.
  One of the keys from the `origins` dictionary in the database's top
  `index.yaml` file. Undefined, if the latter is not defined. Examples:
  `github`, `beaker`, or `suites_zip`. See the `origins` dictionary in the
  database's top `index.yaml` for the available origins and the meanings they
  assign to `location` values.
* `location`: The location of the test's code, with whatever meaning the
  database choses to assign to it, but must be interpreted according to the
  `origin`, if specified. Examples: a tarball URL, a path inside a common
  tarball, a Beaker task name. See the `origins` dictionary in the database's
  top `index.yaml` for the available origins and the meanings they assign to
  `location` values.
* `max_duration_seconds`: Maximum number of seconds the test is allowed to
  run.
* `waived`: True if the test's result should be ignored when summarizing the
  overall run result, eg. because it's new or unstable.
* `role`: The value for the Beaker task's role attribute.
* `environment`: Dictionary with environment variables that should be
  set when running this test.
* `maintainers`: List of strings with the names and emails of the test
  maintainers.

Each guest is an object containing the following attributes with the same
meaning as the host type ones: `ignore_panic`, `partitions_list`,
`kickstart_list`, `kernel_options`, `kernel_options_post`, `preboot_tasks`,
`postboot_tasks`, and `tests`. Each guest also exposes the following
attributes:

* `description`: A human-readable description assigned to the guest.
* `args`: The virt-install arguments defining the guest parameters.

## Custom Jinja2 filters

You can use the custom Jinja2 filter `unindent` to remove any leading
whitespace seen in the first non-empty line from every line in a given
text, which makes it easy to indent e.g. a `ks_append` block and enhance
readability.

For example, in the following code snippet, we have indented the
`ks_append` element with 4 spaces using the `unindent` filter:

```text
{% set ks_append | unindent %}
    <ks_append><![CDATA[
    %post
    set -x
    LVNAME=$(lvs -o lv_name,vg_name --noheadings | awk '/root/ {print $1}')
    VGNAME=$(lvs -o lv_name,vg_name --noheadings | awk '/root/ {print $NF}')
    lvextend -l +100%FREE /dev/${VGNAME}/${LVNAME}
    xfs_growfs /
    %end
    ]]></ks_append>
{% endset %}
```

The leading 4 spaces in the `ks_append` block enhance its
readability. However, when rendering the template, the `unindent` filter
removes those spaces to produce the desired output.

# Copyright (c) 2024 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Integration tests for guest hosts"""
from tests.test_integration import IntegrationTests
from tests.test_integration import assets_mkdir
from tests.test_integration import kpet_run_generate

OUTPUT_TXT_J2 = """
    {%- for scene in SCENES -%}
        {{- "S" -}}
        {%- for recipeset in scene.recipesets -%}
            {{- ("", ":")[loop.first] -}}
            {{- "RS" -}}
            {%- for host in recipeset -%}
                {{- ("", ":")[loop.first] -}}
                {{- "H:" + host.type_name -}}
                {%- for test in host.tests -%}
                    {{- ("", ":")[loop.first] -}}
                    {{- "T:" + test.name -}}
                    {{- (",", "")[loop.last] -}}
                {%- endfor -%}
                {%- for name, guest in host.guests.items() -%}
                    {{- ("", ":")[loop.first] -}}
                    {{- "G:" + name -}}
                    {%- for test in guest.tests -%}
                        {{- ("", ":")[loop.first] -}}
                        {{- "T:" + test.name -}}
                        {{- (",", "")[loop.last] -}}
                    {%- endfor -%}
                    {{- (";", "")[loop.last] -}}
                {%- endfor -%}
                {{- (";", "")[loop.last] -}}
            {%- endfor -%}
            {{- (";", "")[loop.last] -}}
        {%- endfor -%}
        {{- (";", "")[loop.last] -}}
    {%- endfor -%}
"""

INDEX_YAML_HEADER = """\
                arches:
                    - arch
                trees:
                    tree:
                        arches: .*
                template: output.txt.j2
                case:
                    universal_id: test_id
                    maintainers:
                        - name: maint1
                          email: maint1@maintainers.org
                    location: somewhere
                    max_duration_seconds: 100
                    cases:
"""


class IntegrationDomainsTests(IntegrationTests):
    """Integration tests for guest handling"""

    def test_no_guests_dict(self):
        """Check host type without guests dict works fine."""
        assets = {
            "index.yaml": INDEX_YAML_HEADER +
            """
                        x:
                            name: x
                            host_types: a
                        y:
                            name: y
                            host_types: b
                host_types:
                    a: {}
                    b: {}
            """,
            "output.txt.j2": OUTPUT_TXT_J2
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                "--no-lint",
                stdout_equals='S:RS:H:a:T:x;RS:H:b:T:y'
            )

    def test_empty_guests_dict(self):
        """Check host type with empty guests dict works fine."""
        assets = {
            "index.yaml": INDEX_YAML_HEADER +
            """
                        x:
                            name: x
                            host_types: a
                        y:
                            name: y
                            host_types: b
                host_types:
                    a:
                        guests: {}
                    b:
                        guests: {}
            """,
            "output.txt.j2": OUTPUT_TXT_J2
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                "--no-lint",
                stdout_equals='S:RS:H:a:T:x;RS:H:b:T:y'
            )

    def test_unused_guests(self):
        """Check host type with unused guests works fine."""
        assets = {
            "index.yaml": INDEX_YAML_HEADER +
            """
                        x:
                            name: x
                            host_types: a
                        y:
                            name: y
                            host_types: b
                host_types:
                    a:
                        guests:
                            b: {"args": ""}
                    b:
                        guests:
                            a: {"args": ""}
            """,
            "output.txt.j2": OUTPUT_TXT_J2
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                "--no-lint",
                stdout_equals='S:RS:H:a:T:x;RS:H:b:T:y'
            )

    def test_unknown_guest(self):
        """Check test matching an unknown guest produces an error."""
        assets = {
            "index.yaml": INDEX_YAML_HEADER +
            """
                        x:
                            name: x
                            host_types: a
                        y:
                            name: y
                            host_types: b/b
                host_types:
                    a:
                        guests:
                            b: {"args": ""}
                    b:
                        guests:
                            a: {"args": ""}
            """,
            "output.txt.j2": OUTPUT_TXT_J2
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                "--no-lint",
                status=1,
                stderr_matching=""".*Invalid: Host type regex "b/b" """
                """of case /y does not match any of the available """
                """host type names: \\['a', 'a/b', 'b', 'b/a'\\].*"""
            )

    def test_one_guest_test(self):
        """Check one guest test works fine."""
        assets = {
            "index.yaml": INDEX_YAML_HEADER +
            """
                        x:
                            name: x
                            host_types: a/b
                host_types:
                    a:
                        guests:
                            b: {"args": ""}
                    b:
                        guests:
                            a: {"args": ""}
            """,
            "output.txt.j2": OUTPUT_TXT_J2
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                "--no-lint",
                stdout_equals='S:RS:H:a:G:b:T:x'
            )

    def test_host_test_guest_test(self):
        """Check one host test, and one guest test works fine."""
        assets = {
            "index.yaml": INDEX_YAML_HEADER +
            """
                        x:
                            name: x
                            host_types: a
                        y:
                            name: y
                            host_types: a/b
                host_types:
                    a:
                        guests:
                            b: {"args": ""}
                    b:
                        guests:
                            a: {"args": ""}
            """,
            "output.txt.j2": OUTPUT_TXT_J2
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                "--no-lint",
                stdout_equals='S:RS:H:a:T:x:G:b:T:y'
            )

    def test_host_test_other_guest_test(self):
        """Check one host test, and one other host's guest test works fine."""
        assets = {
            "index.yaml": INDEX_YAML_HEADER +
            """
                        x:
                            name: x
                            host_types: a
                        y:
                            name: y
                            host_types: b/a
                host_types:
                    a:
                        guests:
                            b: {"args": ""}
                    b:
                        guests:
                            a: {"args": ""}
            """,
            "output.txt.j2": OUTPUT_TXT_J2
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                "--no-lint",
                stdout_equals='S:RS:H:a:T:x;RS:H:b:G:a:T:y'
            )

    def test_host_or_guest_test(self):
        """Check test matching both host and guest works fine."""
        assets = {
            "index.yaml": INDEX_YAML_HEADER +
            """
                        x:
                            name: x
                            host_types: .*/.*
                        y:
                            name: y
                            host_types: b.*
                host_types:
                    a:
                        guests:
                            a: {"args": ""}
                    b:
                        guests:
                            b: {"args": ""}
            """,
            "output.txt.j2": OUTPUT_TXT_J2
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                "--no-lint",
                stdout_equals='S:RS:H:a:G:a:T:x;RS:H:b:T:y'
            )

    def test_test_matching_two_guests(self):
        """Check test matching two guest works fine."""
        assets = {
            "index.yaml": INDEX_YAML_HEADER +
            """
                        x:
                            name: x
                            host_types: a/.*
                        y:
                            name: y
                            host_types: a/a2
                host_types:
                    a:
                        guests:
                            a1: {"args": ""}
                            a2: {"args": ""}
            """,
            "output.txt.j2": OUTPUT_TXT_J2
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                "--no-lint",
                stdout_equals='S:RS:H:a:G:a1:T:x;G:a2:T:y'
            )

    def test_tests_matching_multiple_guests(self):
        """Check tests matching multiple guests works fine."""
        assets = {
            "index.yaml": INDEX_YAML_HEADER +
            """
                        x:
                            name: x
                            host_types: .*/a.*
                        y:
                            name: y
                            host_types: .*/a.*
                        z1:
                            name: z1
                            host_types: b/a.*
                        z2:
                            name: z2
                            host_types: b/a2
                host_types:
                    a:
                        guests:
                            a1: {"args": ""}
                            a2: {"args": ""}
                            a3: {"args": ""}
                    b:
                        guests:
                            a1: {"args": ""}
                            a2: {"args": ""}
                            a3: {"args": ""}
            """,
            "output.txt.j2": OUTPUT_TXT_J2
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                "--no-lint",
                stdout_equals='S:RS:H:a:G:a1:T:x,T:y;'
                'RS:H:b:G:a1:T:z1;G:a2:T:z2'
            )

    def test_tests_matching_most(self):
        """Check tests matching almost all hosts and guests, one-to-one."""
        assets = {
            "index.yaml": INDEX_YAML_HEADER +
            """
                        x:
                            name: x
                            host_types: a
                        x1:
                            name: x1
                            host_types: a/a1
                        x3:
                            name: x3
                            host_types: .*a3
                        y:
                            name: y
                            host_types: b
                        y2:
                            name: y2
                            host_types: .*b2
                        y3:
                            name: y3
                            host_types: b/b3
                host_types:
                    a:
                        guests:
                            a1: {"args": ""}
                            a2: {"args": ""}
                            a3: {"args": ""}
                    b:
                        guests:
                            b1: {"args": ""}
                            b2: {"args": ""}
                            b3: {"args": ""}
                    c:
                        guests:
                            c1: {"args": ""}
                            c2: {"args": ""}
                            c3: {"args": ""}
            """,
            "output.txt.j2": OUTPUT_TXT_J2
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                "--no-lint",
                stdout_equals='S:'
                'RS:H:a:T:x:G:a1:T:x1;G:a3:T:x3;'
                'RS:H:b:T:y:G:b2:T:y2;G:b3:T:y3'
            )

    def test_lotsa_tests(self):
        """Check tests filling up all available hosts and guests."""
        assets = {
            "index.yaml": INDEX_YAML_HEADER +
            """
                        a1: {"name": "a1", "host_types": "a"}
                        a2: {"name": "a2", "host_types": "a"}
                        a3: {"name": "a3", "host_types": "a"}
                        a4: {"name": "a4", "host_types": "a"}
                        a5: {"name": "a5", "host_types": "a"}
                        aa11: {"name": "aa11", "host_types": "a/a1"}
                        aa12: {"name": "aa12", "host_types": "a/a1"}
                        aa13: {"name": "aa13", "host_types": "a/a1"}
                        aa14: {"name": "aa14", "host_types": "a/a1"}
                        aa15: {"name": "aa15", "host_types": "a/a1"}
                        aa21: {"name": "aa21", "host_types": "a/a2"}
                        aa22: {"name": "aa22", "host_types": "a/a2"}
                        aa23: {"name": "aa23", "host_types": "a/a2"}
                        aa24: {"name": "aa24", "host_types": "a/a2"}
                        aa25: {"name": "aa25", "host_types": "a/a2"}
                        b1: {"name": "b1", "host_types": "b"}
                        b2: {"name": "b2", "host_types": "b"}
                        b3: {"name": "b3", "host_types": "b"}
                        b4: {"name": "b4", "host_types": "b"}
                        b5: {"name": "b5", "host_types": "b"}
                        bb1: {"name": "bb1", "host_types": "b/b"}
                        bb2: {"name": "bb2", "host_types": "b/b"}
                        bb3: {"name": "bb3", "host_types": "b/b"}
                        bb4: {"name": "bb4", "host_types": "b/b"}
                        bb5: {"name": "bb5", "host_types": "b/b"}
                        c1: {"name": "c1", "host_types": "c"}
                        c2: {"name": "c2", "host_types": "c"}
                        c3: {"name": "c3", "host_types": "c"}
                        c4: {"name": "c4", "host_types": "c"}
                        c5: {"name": "c5", "host_types": "c"}
                host_types:
                    a:
                        guests:
                            a1: {"args": ""}
                            a2: {"args": ""}
                    b:
                        guests:
                            b: {"args": ""}
                    c: {}
            """,
            "output.txt.j2": OUTPUT_TXT_J2
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                "--no-lint",
                stdout_equals='S:'
                'RS:H:a:T:a1,T:a2,T:a3,T:a4,T:a5:'
                'G:a1:T:aa11,T:aa12,T:aa13,T:aa14,T:aa15;'
                'G:a2:T:aa21,T:aa22,T:aa23,T:aa24,T:aa25;'
                'RS:H:b:T:b1,T:b2,T:b3,T:b4,T:b5:'
                'G:b:T:bb1,T:bb2,T:bb3,T:bb4,T:bb5;'
                'RS:H:c:T:c1,T:c2,T:c3,T:c4,T:c5'
            )

    def test_guest_attrs(self):
        """Check guests accept their attributes, but nothing else."""
        assets = {
            "index.yaml": INDEX_YAML_HEADER +
            """
                        x:
                            name: x
                            host_types: a/a
                        y:
                            name: y
                            host_types: b
                host_types:
                    b: {}
                    a:
                        guests:
                            a:
                                description: Guest A
                                args: --mem=16G --cpu=4
                                ignore_panic: True
                                partitions: partitions.xml.j2
                                kickstart: kickstart.xml.j2
                                kernel_options: opt1 opt2
                                kernel_options_post: post_opt1 post_opt2
                                preboot_tasks: preboot.xml.j2
                                postboot_tasks: postboot.xml.j2
            """,
            "output.txt.j2": OUTPUT_TXT_J2
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                "--no-lint",
                stdout_equals='S:RS:H:b:T:y;RS:H:a:G:a:T:x'
            )

        attr_print_assets = assets.copy()
        attr_print_assets["output.txt.j2"] = """
            {%- for scene in SCENES -%}
                {%- for recipeset in scene.recipesets -%}
                    {%- for host in recipeset -%}
                        {%- for name, guest in host.guests.items() -%}
                            {{- "description: " +
                                guest.description + "\n" -}}
                            {{- "args: " +
                                guest.args + "\n" -}}
                            {{- "ignore_panic: " +
                                guest.ignore_panic | string + "\n" -}}
                            {{- "partitions_list: " +
                                guest.partitions_list | string + "\n" -}}
                            {{- "kickstart_list: " +
                                guest.kickstart_list | string + "\n" -}}
                            {{- "kernel_options: " +
                                guest.kernel_options + "\n" -}}
                            {{- "kernel_options_post: " +
                                guest.kernel_options_post + "\n" -}}
                            {{- "preboot_tasks: " +
                                guest.preboot_tasks + "\n" -}}
                            {{- "postboot_tasks: " +
                                guest.postboot_tasks + "\n" -}}
                        {%- endfor -%}
                    {%- endfor -%}
                {%- endfor -%}
            {%- endfor -%}
        """
        with assets_mkdir(attr_print_assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                "--no-lint",
                stdout_equals=""
                "description: Guest A\n"
                "args: --mem=16G --cpu=4\n"
                "ignore_panic: True\n"
                "partitions_list: ['partitions.xml.j2']\n"
                "kickstart_list: ['kickstart.xml.j2']\n"
                "kernel_options: opt1 opt2\n"
                "kernel_options_post: post_opt1 post_opt2\n"
                "preboot_tasks: preboot.xml.j2\n"
                "postboot_tasks: postboot.xml.j2\n"
            )

        invalid_assets = assets.copy()
        invalid_assets["index.yaml"] += """
                                hostname: example.com
        """
        with assets_mkdir(invalid_assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                "--no-lint",
                status=1,
                stderr_matching=".*Invalid: Invalid guest:\\s+"
                "Invalid: Unexpected members encountered: \\{'hostname'\\}.*"
            )

    def test_host_attrs(self):
        """Check hosts don't accept guest-specific attrs."""
        assets = {
            "index.yaml": INDEX_YAML_HEADER +
            """
                        x:
                            name: x
                            host_types: a
                host_types:
                    a:
                        args: --mem=16G --cpu=4
            """,
            "output.txt.j2": OUTPUT_TXT_J2
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                "--no-lint",
                status=1,
                stderr_matching=".*Invalid: Invalid host type:\\s+"
                "Invalid: Unexpected members encountered: \\{'args'\\}.*"
            )

# Copyright (c) 2021 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Integration tests for "universal_id" property of test cases"""
from tests.test_integration import IntegrationTests
from tests.test_integration import assets_mkdir
from tests.test_integration import kpet_run_generate

OUTPUT_TXT_J2 = """
{%- for scene in SCENES -%}
    {%- for recipeset in scene.recipesets -%}
        {% for HOST in recipeset %}
            {% for test in HOST.tests %}
                {{ test.name }}: {{ test.waived }}
            {% endfor %}
        {% endfor %}
    {%- endfor -%}
{%- endfor -%}
"""


class IntegrationUniversalIdTests(IntegrationTests):
    """Integration tests for "universal_id" property of test cases"""

    def test_must_be_present(self) -> None:
        """Check we get an Exception when "universal_id" is missing"""
        assets = {
            "index.yaml": """
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                  maintainers:
                    - name: maint1
                      email: maint1@maintainers.org
                  cases:
                    A:
                      name: A
                """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path,
                status=1,
                stderr_matching=r'.* has no universal_id specified\b.*'
            )
